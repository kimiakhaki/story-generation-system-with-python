import glob
import markovify
import re
import time
import numpy as np
from nltk.corpus import gutenberg
from nltk.tokenize import sent_tokenize
from nltk import ngrams
from nltk.translate.bleu_score import sentence_bleu
from memory_profiler import memory_usage

def text_cleaner(text):
  text = re.sub(r'--', ' ', text)
  text = re.sub('[\[].*?[\]]', '', text)
  text = re.sub(r'(\b|\s+\-?|^\-?)(\d+|\d*\.\d+)\b','', text)
  text = ' '.join(text.split())
  return text

def compute_coherence(story, n):
    sentences = sent_tokenize(story)
    overlaps = []
    for i in range(len(sentences) - 1):
        ngrams1 = set(ngrams(sentences[i].split(), n))
        ngrams2 = set(ngrams(sentences[i+1].split(), n))
        overlap = ngrams1 & ngrams2
        if len(ngrams1) > 0 & len(ngrams2):
         overlaps.append(len(overlap) / min(len(ngrams1), len(ngrams2)))
    return sum(overlaps) / len(overlaps)

def calculate_selfBleu(generated_texts):
    scores = []
    for i in range(len(generated_texts)):
        references = [text for j, text in enumerate(generated_texts) if j != i]
        candidate = generated_texts[i]
        scores.append(sentence_bleu(references, candidate))
    return np.mean(scores)

start_time = time.time()
mem_usage_before = memory_usage()[0]
text = ""
directory = "C:/Users/Kimia/AppData/Roaming/nltk_data/corpora/gutenberg/New Folder"
for filename in glob.glob(directory + "/*.txt"):
     with open(filename, encoding="unicode_escape") as file:
        text += file.read()


text = re.sub(r'Chapter \d+', '', text)
text = text_cleaner(text)

model = markovify.Text(text, state_size=3)

story = ""
for i in range(10):
        sentence = model.make_short_sentence(80)
        while sentence is None:
         sentence = model.make_short_sentence(80)
        story += sentence + " "

end_time = time.time()
mem_usage_after = memory_usage()[0]

coherence = compute_coherence(story, 3)
self_blue = calculate_selfBleu(story)

print(story)
print(f"Coherence: {coherence}")
print(f"Self_blue: {self_blue}")
print(f'Time taken: {(end_time - start_time)/60 } mins')
print(f'Memory used: {mem_usage_after - mem_usage_before} MiB')